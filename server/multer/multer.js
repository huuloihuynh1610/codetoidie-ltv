const multer = require('multer');
const path = require('path');

const MEGABYTE = 1024 * 1024;
const MAX_UPLOAD_FILE_SIZE_MB = 25;
const MAX_UPLOAD_FILE_SIZE_BYTE = MAX_UPLOAD_FILE_SIZE_MB * MEGABYTE;
const USER_AVATAR_DESTINATION = path.resolve(__dirname, '../../public/file');

const storageUserAvatar = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, USER_AVATAR_DESTINATION);
    },
    filename: async function (req, file, cb) {
      const originalName = file.originalname;
      const fileExtension = path.extname(originalName) || '';
      const finalName = `${req.params.name}${file.originalname}`;
      cb(null, finalName);
    }
  });
  const limits = { fileSize: MAX_UPLOAD_FILE_SIZE_BYTE };
  const uploadUserAvatar = multer({
    storage: storageUserAvatar,
    limits: limits,
    fileFilter: function (req, file, cb) {
      const originalName = file.originalname.toLowerCase();
      return cb(null, true);
    },
  });
const userAvatarUploader = uploadUserAvatar.single('myFile');
module.exports = {
    userAvatarUploader
}