// built-in NodeJS
const path = require('path');
const http = require('http');
const fs = require('fs');

const express = require('express');
const socketIO = require('socket.io');
const bodyParser = require('body-parser');
const session = require('express-session');
const connectFlash = require('connect-flash');
const cron = require('node-cron');
const rimraf = require("rimraf");

const { generateMessage, generateLocation, typingTemplate } = require('./helper/messageTemplate');
const Room = require('./models/Room');
const { userAvatarUploader } = require('../server/multer/multer');

const app = express();
const server = http.createServer(app);

const io = socketIO(server);
const configSession = (app) => {
  app.use(session({
      key: "express.sid",
      secret: "huuloi1610",
      resave: true,
      saveUninitialized: false,
      cookie: {
          maxAge: 1000*60*60*24
      }
  }))
} 
configSession(app)
app.use(connectFlash());
app.use(bodyParser.json({ limit: '20mb' }));
app.use(bodyParser.urlencoded({ limit: '20mb', extended: false }));

const publicPath = path.join(`${__dirname}/../public`)
app.use(express.static(publicPath))

// fs.readdir(fileImg, (err, files) => {
//   console.log(files)
//   files.forEach(file => {
//     console.log(file);
//   });
// });
// var dir = './public/file';

// cron.schedule('0 1 * * *', () => {
//   rimraf("./public/file", function () {
//     if (!fs.existsSync(dir)){
//       fs.mkdirSync(dir);
//   }
//   });
// }, {
//   scheduled: true,
//   timezone: "Europe/London"
// });
// const filePath = path.resolve('./public/file');
// fs.unlinkSync(filePath);

app.set("view engine", "ejs");
app.set("views", "./server/view");
const newRoom = new Room();
let userSeen = []
io.on("connection", (socket) => {
  socket.on('clientConnection', msg => {
    const { name, room, password } = msg;
    const user = {
      id: socket.id,
      name, room,
      password,
      listSeen: []
    }
    newRoom.createUser(user)
    socket.join(room);

    socket.on("clientMessage", msg => {
      newRoom.users.forEach(item => {
        item.listSeen = []
      })
      io.to(room).emit("fromServer", generateMessage(msg.from, msg.content))
    })
    socket.on("sendLocation", msg => {
      newRoom.users.forEach(item => {
        item.listSeen = []
      })
      io.to(room).emit("sendLocationToOthers", generateLocation(msg.from, msg.lat, msg.lng))
    })

    socket.on("typingFromClient", msg => {
      socket.broadcast.to(room).emit("typingFromServer", typingTemplate(msg.name, msg.content));
    })
    socket.on("stopTypeFromClient", msg => {
      socket.broadcast.to(room).emit("stopTypingFromServer", typingTemplate(msg.name, msg.content));
    })

    socket.broadcast.to(room).emit("fromServer", generateMessage("Admin", `${name} joined room`))
    io.to(room).emit("userList", { userList: newRoom.users })
    socket.emit("fromServer", generateMessage("Admin", "Welcome to chat app"))

    socket.on("clientSendFileToServer", data => {
      newRoom.users.forEach(item => {
        item.listSeen = []
      })
      io.to(room).emit("serverSendFileToClient", data)
    })

    socket.on("seen", data => {
      socket.to(room).emit("seenFromServer", data);
    })
    // list user seen
    socket.on("hasSeen", data => {  
      newRoom.users.forEach(s => {
        if (s.room === data.room) {
          if (s.listSeen.length > 0) {
            if (s.listSeen.indexOf(data.name) === - 1) {
              s.listSeen.push(data.name);
            }
          } else {
            s.listSeen.push(data.name);  
          }
        }
      })
      io.to(room).emit("serverSendHasSeen", newRoom);
    });
    //
    socket.on("disconnect", () => {
      newRoom.deleteUserById(socket.id);
      io.to(room).emit("userList", { userList: newRoom.users })
      io.to(room).emit("fromServer", generateMessage("Admin", `${name} left`))
    })
  })
})
app.get('/', (req, res) => {
  return res.render('index', {
    error: req.flash('error')
  });
})
app.get('/chat', (req, res) => {
  return res.redirect('back');
})
app.post('/file/:name', userAvatarUploader)
app.post('/chat', (req, res) => {
  const room = req.body.room;
  const currentRoom = newRoom.users.filter(r => r.room === room);
  if (currentRoom.length === 0) {
    return res.render('chat', {name: req.body.name, room: req.body.room, password: req.body.password, userSeen: userSeen});
  } else {
    const checkPassword = currentRoom[0].password;
    if (checkPassword === req.body.password) {
      return res.render('chat', {name: req.body.name, room: req.body.room, password: req.body.password, userSeen: userSeen}); 
    } else {
      req.flash('error', {error: "Wrong password !!!!"});
      return res.redirect('back');
    }
  }
})

// run port
const port = process.env.PORT || 5000;
server.listen(port, () => {
  console.log(`App is running on port ${port}`)
})