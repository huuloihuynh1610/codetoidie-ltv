const socket = io();

socket.on('connect', () => {
  const { name, room } = $.deparam(window.location.search);
  socket.emit('clientConnection', {
    name, room
  })
})

socket.on("fromServer", msg => {
  const { name } = $.deparam(window.location.search);
  if (name === msg.from) {
    let template = $("#message-template-me").html();
    let html = Mustache.render(template, {
      from: msg.from,
      createdAt: moment(msg.createdAt).format('h:mm a'),
      content: msg.content
    })
    $("#messages").append(html);
  } else {
    let template = $("#message-template").html();
    let html = Mustache.render(template, {
      from: msg.from,
      createdAt: moment(msg.createdAt).format('h:mm a'),
      content: msg.content
    })
    $("#messages").append(html);
  }
})

socket.on('disconnect', () => {
  console.log("Disconect !!!")
})

$("#message-form").on("submit", (e) => {
  e.preventDefault();
  const { name } = $.deparam(window.location.search);

  socket.emit("clientMessage", {
    from: name,
    content: $("[name=message]").val(),
    createdAt: new Date()
  })

  $("[name=message]").val("")
})

$("#send-location").on("click", () => {
  const { name } = $.deparam(window.location.search);
  if (!navigator.geolocation) return alert("Your browser does not support GEOLOCATION")

  navigator.geolocation.getCurrentPosition(position => {
    socket.emit("sendLocation", {
      from: name,
      lat: position.coords.latitude,
      lng: position.coords.longitude
    })
  })
})

socket.on("sendLocationToOthers", msg => {
  const olTag = $("#messages");

  let liTag = $(`<li></li>`)
  let aTag = $(`<a>My location</a>`)
  aTag.attr("href", msg.url)
  aTag.attr("target", "_blank")
  liTag.append(aTag)

  olTag.append(liTag)
})

socket.on("userList", msg => {
  const { userList } = msg
  const { room } = $.deparam(window.location.search);
  const olTag = $('<ol></ol>')
  const listMem = userList.filter(r => {
    return r.room == room;
  })
  listMem.forEach(user => {
    const liTag = $(`<li>${user.name}</li>`)
    olTag.append(liTag)
  })

  $("#users").html(olTag)
})

$("#messagesType").on("keyup", function() {
  const { name } = $.deparam(window.location.search);
  if ($("#messagesType").val() !== "") {
    socket.emit("typingFromClient", {
      name: name,
      content: "isTyping"
    })
  } else {
    socket.emit("stopTypeFromClient", {
      name: name,
      content: "stopTyping"
    })
  }
})

socket.on("typingFromServer", data => {
  $("#typings").show();
  console.log(data);
  $("#typings").html(`<p>${data.name} is typing</p>`)
});

socket.on("stopTypingFromServer", data => {
  $("#typings").hide();
})

$("#sendFile").on("click", function() {
  const { name } = $.deparam(window.location.search);
  const xhr = new XMLHttpRequest();
  const formData = new FormData();  
  const inpFile = document.getElementById("inpFile");
  const subName = `asd${inpFile.files[0].name}`;
  inpFile.files[0].subName = "qwe";
  for (const file of inpFile.files) {
    formData.append('myFile', file)
  }
  let r = Math.random().toString(36).substring(10);
  xhr.open("post", `/file/${r}`);
  xhr.send(formData);
  const data = {
    file: inpFile.files[0].name,
    subName: `${r}${inpFile.files[0].name}`,
    name: name,
    type: inpFile.files[0].type
  }
  socket.emit("clientSendFileToServer", data)
})

socket.on("serverSendFileToClient", data => {
  if (data.type === 'image/jpeg' || data.type === 'image/png') {
    let template = $("#file-image-template").html();
    let html = Mustache.render(template, {
      name: data.name,
      content: `${data.file}`,
      link: `${data.subName}`,
      createdAt: moment(new Date()).format('h:mm a')
    })
    $("#messages").append(html);
  } else {
    let template = $("#file-template").html();
    let html = Mustache.render(template, {
      name: data.name,
      content: `${data.file}`,
      link: `${data.subName}`,
      createdAt: moment(new Date()).format('h:mm a')
    })
    $("#messages").append(html);
    }
 
})