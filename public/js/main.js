
  const socket = io();

socket.on('connect', () => {
 const name = $("#username").val();
 const room = $("#roomchat").val();
 const password = $("#password").val();
  socket.emit('clientConnection', {
    name, room, password
  })
})

socket.on("fromServer", msg => {
  const name = $("#username").val();
  const room = $("#roomchat").val();
  if (name === msg.from) {
    let template = $("#message-template-me").html();
    let html = Mustache.render(template, {
      from: msg.from,
      createdAt: moment(msg.createdAt).format('h:mm a'),
      content: msg.content
    })
    $("#messages").append(html);
    var messageBody  = document.getElementById("messages");
    const w = messageBody.scrollHeight
    messageBody.scrollTop = w - messageBody.clientHeight;
  } else {
    let template = $("#message-template").html();
    let html = Mustache.render(template, {
      from: msg.from,
      createdAt: moment(msg.createdAt).format('h:mm a'),
      content: msg.content
    })
    $("#messages").append(html);
    var messageBody  = document.getElementById("messages");
    const w = messageBody.scrollHeight
    messageBody.scrollTop = w - messageBody.clientHeight;
  }
  if (msg.from !== name) {
    // console.log("co tin nhan toi ne");
    if (document.hasFocus()) {
      socket.emit('seen', {name: name, room: room });
    }
  }
})

socket.on("seenFromServer", data => {
  console.log(`${data.name} seen`);
})

socket.on('disconnect', () => {
  console.log("Disconect !!!")
})

$("#message-form").on("submit", (e) => {
  e.preventDefault();
  const name = $("#username").val();
  if($("[name=message]").val()){
    socket.emit("clientMessage", {
      from: name,
      content: $("[name=message]").val(),
      createdAt: new Date()
    })
  
    $("[name=message]").val("")
    var messageBody  = document.getElementById("messages");
    const w = messageBody.scrollHeight
    messageBody.scrollTop = w - messageBody.clientHeight;
  }
})

$("#send-location").on("click", () => {
  const name = $("#username").val();
  if (!navigator.geolocation) return alert("Your browser does not support GEOLOCATION")

  navigator.geolocation.getCurrentPosition(position => {
    socket.emit("sendLocation", {
      from: name,
      lat: position.coords.latitude,
      lng: position.coords.longitude
    })
  })
})

socket.on("sendLocationToOthers", msg => {

  const name = $("#username").val();
  if (msg.from == name) {
    let template = $("#location-template-me").html();
    let html = Mustache.render(template, {
      from: msg.from,
      createdAt: moment(msg.createdAt).format('h:mm a'),
      content: "My Location",
      link : msg.url
    })
    $("#messages").append(html)
  } else {
    let template = $("#location-template").html();
    let html = Mustache.render(template, {
      from: msg.from,
      createdAt: moment(msg.createdAt).format('h:mm a'),
      content: "My Location",
      link : msg.url
    })
    $("#messages").append(html)
  }
})

socket.on("userList", msg => {
  const { userList } = msg
  const name = $("#username").val();
  const room = $("#roomchat").val();
  const olTag = $('<ol></ol>')
  const listMem = userList.filter(r => {
    return r.room == room;
  })
  listMem.forEach((user, index) => {
    if (name === user.name) {
      if (index === 0) {
        const liTag = $(`<li style="color: #ffffff; white-space: nowrap; max-width: 180px; overflow: hidden; text-overflow: ellipsis;"> <i style="color: yellow" class="fas fa-star"></i> ${user.name}</li>`)
        olTag.append(liTag)
      } else {
        const liTag = $(`<li style="color: #ffffff; white-space: nowrap; max-width: 180px; overflow: hidden; text-overflow: ellipsis;">${user.name}</li>`)
        olTag.append(liTag)
      }
      
    } else {
      if (index === 0) {
        const liTag = $(`<li style=" white-space: nowrap; max-width: 180px; overflow: hidden; text-overflow: ellipsis;" ><i style="color: yellow;" class="fas fa-star"></i> ${user.name}</li>`)
        olTag.append(liTag)
      } else {
        const liTag = $(`<li style="white-space: nowrap; max-width: 180px; overflow: hidden; text-overflow: ellipsis;">${user.name}</li>`)
        olTag.append(liTag)
      }
   
    }
  })

  $("#users").html(olTag)
})

$("#messagesType").on("keyup", function() {
  const name = $("#username").val();
  if ($("#messagesType").val() !== "") {
    socket.emit("typingFromClient", {
      name: name,
      content: "isTyping"
    })
  } else {
    socket.emit("stopTypeFromClient", {
      name: name,
      content: "stopTyping"
    })
  }
})

socket.on("typingFromServer", data => {
  $("#typings").show();
  // console.log(data);
  $("#typings").html(`  <div class="ticontainer" style="display: flex">
  <span style="color: red"> ${data.name}  </span>
  <div class="tiblock">
 
      <pre> is typing </pre>
      <div class="tidot"></div>
      <div class="tidot"></div>
      <div class="tidot"></div>
      <div class="tidot"></div>
  </div>
</div>` )
});

socket.on("stopTypingFromServer", data => {
  $("#typings").hide();
})

$("#sendFile").on("click", function() {
  const name = $("#username").val();
  const xhr = new XMLHttpRequest();
  const formData = new FormData();  
  const inpFile = document.getElementById("inpFile");
  const subName = `asd${inpFile.files[0].name}`;
  inpFile.files[0].subName = "qwe";
  for (const file of inpFile.files) {
    formData.append('myFile', file)
  }
  let r = Math.random().toString(36).substring(10);
  xhr.open("post", `/file/${r}`);
  xhr.send(formData);
  const data = {
    file: inpFile.files[0].name,
    subName: `${r}${inpFile.files[0].name}`,
    name: name,
    type: inpFile.files[0].type
  }
  socket.emit("clientSendFileToServer", data)
  var messageBody  = document.getElementById("messages");
  const w = messageBody.scrollHeight
  messageBody.scrollTop = w - messageBody.clientHeight;
})

socket.on("serverSendFileToClient", data => {
  const name = $("#username").val();
  if (data.type === 'image/jpeg' || data.type === 'image/png') {
    if(data.name == name) {
      let template = $("#file-image-template-me").html();
      let html = Mustache.render(template, {
        name: data.name,
        content: `${data.file}`,
        link: `${data.subName}`,
        createdAt: moment(new Date()).format('h:mm a')
      })
      $("#messages").append(html);
      var messageBody  = document.getElementById("messages");
      const w = messageBody.scrollHeight
      messageBody.scrollTop = w - messageBody.clientHeight;
    } else {
      let template = $("#file-image-template").html();
      let html = Mustache.render(template, {
        name: data.name,
        content: `${data.file}`,
        link: `${data.subName}`,
        createdAt: moment(new Date()).format('h:mm a')
      })
      $("#messages").append(html);
      var messageBody  = document.getElementById("messages");
      const w = messageBody.scrollHeight
      messageBody.scrollTop = w - messageBody.clientHeight;
    }
    } else {
      if(data.name == name) {
          let template = $("#file-template-me").html();
        let html = Mustache.render(template, {
          name: data.name,
          content: `${data.file}`,
          link: `${data.subName}`,
          createdAt: moment(new Date()).format('h:mm a')
        })
        $("#messages").append(html);
        var messageBody  = document.getElementById("messages");
        const w = messageBody.scrollHeight
        messageBody.scrollTop = w - messageBody.clientHeight;
        
      }else {
        let template = $("#file-template").html();
      let html = Mustache.render(template, {
        name: data.name,
        content: `${data.file}`,
        link: `${data.subName}`,
        createdAt: moment(new Date()).format('h:mm a')
      })
      $("#messages").append(html);
      var messageBody  = document.getElementById("messages");
      const w = messageBody.scrollHeight
      messageBody.scrollTop = w - messageBody.clientHeight;
      }
      }
  })

function checkSeen() {
  setInterval(() => {
    if (document.hasFocus()) {
      const name = $("#username").val();
      const room = $("#roomchat").val();
      socket.emit("hasSeen", {name: name, room: room });
    }
  }, 2000)
}
checkSeen()
socket.on("serverSendHasSeen", data => {
  const name = $("#username").val();
  const room = $("#roomchat").val();
  let list = ""
  if (data.users.length > 0) {
    let ar = data.users.filter(item => item.room == room);
    ar[0].listSeen.forEach((item, index) => {
      if(index === 0) {
        if(item !== name) {
          list += `${item} `;
        }
      }else {
        if(item !== name) {
          list += `, ${item} `;
        }
      }
     if (list.length > 0 ) {
      $("#userSeen").html(`Seen by ${list} <i class="fa fa-eye" aria-hidden="true"></i>`)
     } else {
      $("#userSeen").html(``);
     }
    })
  } else {
    $("#userSeen").html(` `);
  }
})


